/*  process_image_simd.S
 
    MI01 - TP Assembleur 2 à 5

    Réalise le traitement d'une image bitmap 32 bits par pixel.
    Implémentation SIMD
*/

.file "process_image_simd.S"
.intel_syntax noprefix

.text

/***********************************************************************
  Sous-programme process_image_simd
    
  Réalise le traitement d'une image 32 bits.
  
  Le passage des paramètres respecte les conventions x64 sous Linux
  
  Par registres :
            Largeur de l'image : rdi
            Hauteur de l'image : rsi
            Pointeur sur l'image source : rdx
            Pointeur sur l'image tampon 1 : rcx
            Pointeur sur l'image tampon 2 : r8
            Pointeur sur l'image finale : r9  
  
  Les registes rbx, rbp, r12, r13, r14, et r15 doivent être sauvegardés
  si vous les utilisez (sauvegarde par l'appelé). Les autres registres 
  peuvent être modifiés sans risque (sauvegardés par l'appelant).
***********************************************************************/

.global process_image_simd
process_image_simd:  
            push    rbp
            mov     rbp, rsp

             # Calcul du nombre de pixels de l'image dans rdi.
            push    rdi         /* Sauvegarde de la largeur de l'image */
            imul    rdi, rsi    /* rdi <- largeur x hauteur */


            /* On itère sur chaque pixel en partant du dernier pixel
            jusqu'au premier pixel. rdi contient en fait le nombre de
            pixels restants à traiter. */

            pxor		xmm3, xmm3
            pcmpeqq		xmm2, xmm2
            pslld		xmm2, 24

			mov			r11, 0x00095b1b00095b1b
			movq		xmm1, r11
			punpcklqdq	xmm1, xmm1

loop_gs:

            /***********************************************************
              Ajoutez votre code pour la partie 1 (niveaux de gris) ici
             **********************************************************/
			movdqa		xmm0, [rdx + rdi * 4 - 16]

			pmaddubsw 	xmm0, xmm1
			phaddsw		xmm0, xmm0
			punpcklwd	xmm0, xmm3

			psrld		xmm0, 7
			paddd		xmm0, xmm2

			movdqa		[rcx + rdi * 4 - 16], xmm0

            sub     	rdi, 4        # 4 pixels de moins à traiter
            ja     		loop_gs


            pop     	rdi           # rdi <- largeur de l'image en pixels

            pop     	rbp
            ret
